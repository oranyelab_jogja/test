# Installation

## Requirements
If your server is not compatible with the requirements below, please upgrade, CMS may not support certain legacy systems.

* Apache HTTP Server
* mod_rewrite
* PHP 5.5+
  * curl
  * gd
  * finfo
  * pdo_mysql
  * mcrypt
* MySQL 5.2+

----------

## Installation

### Step by Step

Below is the process for manually install

1. **Backup your database**: In addition to scheduled backups, you should always create another complete database backup directly before making any broad CMS/database changes
2. Download and unzip the cms_truemoney folder & import tmw_cms.sql to your database
3. Configuring database connection in `/api/config.php` files.
4. Run the migration script to update your database/schema. 

#### Upgrade Database schema:

```bash
php bin/directus db:upgrade
```

#### Troubleshooting

